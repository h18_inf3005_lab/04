#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright 2017 Jacques Berger
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Basé sur le code de Jacques Berger disponible à l'adresse suivante :
# https://github.com/jacquesberger/exemplesINF3005/tree/master/Flask/login

# Ajout de commentaires et modifications de fonctions par Luis-Gaylor Nobre.

from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
# notez que j'importe, grâce à la ligne ci-dessous, mon fichier database.py et donc ma classe Database
from .database import Database

app = Flask(__name__)

# TODO
# Vous devez définir une fonction pour vous connecter à votre base de données..
# basez-vous sur cet exemple de Jacques https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/db/index.py


@app.route('/confirmation')
def confirmation_page():
    return render_template('confirmation.html')


@app.route('/formulaire')
def formulaire_page():
    return render_template('formulaire.html')


@app.route('/personnes')
def personnes_page():
    return render_template('personnes.html')


@app.route('/envoyer', methods=["POST"])
def formulaire_creation():
    name = request.form["name"]
    fname = request.form["fname"]
    age = request.form["age"]
    champs = {}
    champs["name"] = name
    champs["fname"] = fname
    champs["age"] = age
    error = {}
    # Vérification simple des champs non-vides.
    if name == "" or fname == "" or age == "":
        error["vide"] = "Tous les champs sont obligatoires."
    elif age < 1 and age > 125:
        error["age"] = "L'âge doit être entre 0 et 125 inclusivement."
    # Continuer les validations ici...
    if error:
        return render_template("formulaire.html", error=error, champs=champs)
    else:
        # Dans votre fichier database.py vous allez créer une fonction insert_personne...
        # get_db est la fonction que vous devrez définir en début de ce fichier permettant d'identifier votre base de données
        # cette fonctione est basée sur la classe Database() que vous aurez définie dans votre fichier database.py.
        get_db().insert_personne(name, fname, age)
        return redirect("/confirmation")
