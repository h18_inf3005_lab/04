# Étapes de résolution de ce laboratoire 0

### Création de votre base de données.
#### Si vous avez besoin d'un rafraîchissement sur la gestion de base de données, regardez le deuxième laboratoire:
https://gitlab.com/h18_inf3005_lab/02

En guise de rappel:
1. Installez SQLite3 si ce n'est pas déjà fait: https://www.tutorialspoint.com/sqlite/sqlite_installation.htm
2. Adaptez le script sql dans le fichier musique.sql afin de créer une Personne
3. Ce qui vous intéresse le plus est le début du script qui débute par "create table"
4. Réfléchissez à ce que devraient être vos champs nom et prénom (des caractères) et âge.

### Adaptez ce que vous avez fait au laboratoire 3 afin de créer vos routes ou utilisez les routes que j'ai effectuées dans 04_solution.py:
https://gitlab.com/h18_inf3005_lab/03

### Il vous faut faire le code de validation et le code de création d'une personne dans la DB.
1. Adaptez ce fichier en suivant l'exemple de Jacques ici:
https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/db/index.py
2. Créez votre classe Database au sein du fichier databse.py inclus dans ce projet.
3. C'est elle qui se connectera à la base de données pour insérer des données ou pour les afficher !
4. Basez-vous sur cet exemple de Jacques : https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/db/database.py

### Je vous ai montré comment afficher un dictionnaire dans un template Jinja dans formulaire.html

### Maintenant, faites la page d'affichage des personnes basé sur le même exemple de Jacques donné plus haut.

### Apprenez à vous servir du debugger de Flask:
http://flask.pocoo.org/docs/0.12/quickstart/#debug-mode
1. Comprenez que lorsque vous avez une erreur, les lignes d'erreur en haut du message sont celles reliées à l'environnement Flask.
2. Les lignes en bas sont reliées à votre code.

# C'est tout ! Bonne chance et à lundi !
